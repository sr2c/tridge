import hashlib
from time import time
from typing import Optional

from phonenumbers import PhoneNumber, format_number, PhoneNumberFormat


def daystamp() -> int:
    """
    The number of days since the UNIX epoch, as a string
    with base-10 representation.
    """
    return int(time()) // 86400


def e164(pn: PhoneNumber) -> str:
    """
    A canonical representation of a given phone number (using E164 format).

    :param pn: The phone number.
    """
    return format_number(pn, PhoneNumberFormat.E164)


def request_id(pn: PhoneNumber, *, daystamp_override: Optional[int] = None) -> int:
    """
    An integer ID for a given request based on the day and the phone number in use.

    :param daystamp_override: Optionally override the value used for the daystamp.
    :param pn: The phone number.
    """
    cid = e164(pn)
    day = daystamp_override or daystamp()
    digest = hashlib.sha224(f"{str(day)}{cid}".encode('utf-8')).digest()
    return int.from_bytes(digest, byteorder='little')
