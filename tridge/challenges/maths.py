from enum import Enum
import logging

from phonenumbers import PhoneNumber

from tridge.challenges.base import BaseChallenge
from tridge.utils import request_id


class MathsOperation(Enum):
    ADD = 0
    SUBTRACT = 1
    MULTIPLY = 2


symbol = {
    MathsOperation.ADD: "+",
    MathsOperation.SUBTRACT: "-",
    MathsOperation.MULTIPLY: "x"
}


def solve_problem(first_number: int, operation: MathsOperation, second_number: int) -> int:
    if operation == MathsOperation.ADD:
        return first_number + second_number
    if operation == MathsOperation.SUBTRACT:
        return first_number - second_number
    if operation == MathsOperation.MULTIPLY:
        return first_number * second_number


def format_problem(first_number: int, operation: MathsOperation, second_number: int) -> str:
    return f"{first_number} {symbol[operation]} {second_number} = ?"


def generate_problem(pn):
    rid = request_id(pn)
    first_number = rid % 10
    second_number = (rid // 10) % 10
    operation = MathsOperation((rid // 100) % 3)
    problem = first_number, operation, second_number
    logging.debug("Generated maths problem for %s: %s", pn, problem)
    return problem


class MathsChallenge(BaseChallenge):

    def __init__(self):
        super().__init__()

    def verify(self, pn) -> str:
        return str(solve_problem(*generate_problem(pn)))

    def generate(self, pn: PhoneNumber):
        problem = generate_problem(pn)
        return "What is the answer to this maths question?", "text/plain", format_problem(*problem)
