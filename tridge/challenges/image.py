import logging
from typing import Tuple

from captcha.image import ImageCaptcha
from phonenumbers import PhoneNumber

from tridge.challenges.base import BaseChallenge
from tridge.utils import request_id


class ImageChallenge(BaseChallenge):
    _captcha: ImageCaptcha

    def __init__(self):
        self._captcha = ImageCaptcha(fonts=['zxx-sans.ttf'])

    def verify(self, pn: PhoneNumber) -> str:
        code = str(request_id(pn) % 10000)
        logging.debug("Generated an image captcha for %s: %s", pn, code)
        return code

    def generate(self, pn: PhoneNumber) -> Tuple[str, str, bytearray]:
        data = self._captcha.generate(self.verify(pn)).read()
        return "Please enter the number shown in the image.", "image/png", data
