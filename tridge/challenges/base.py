from typing import Optional, Tuple

from phonenumbers import PhoneNumber


class BaseChallenge:

    def generate(self, pn: PhoneNumber) -> Tuple[str, Optional[str], Optional[bytes]]:
        raise NotImplementedError()

    def verify(self, pn) -> str:
        raise NotImplementedError()
