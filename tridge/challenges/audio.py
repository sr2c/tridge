import logging
from typing import Tuple

from captcha.audio import AudioCaptcha
from phonenumbers import PhoneNumber

from tridge.challenges.base import BaseChallenge
from tridge.utils import request_id


class AudioChallenge(BaseChallenge):
    _captcha: AudioCaptcha

    def __init__(self):
        self._captcha = AudioCaptcha()

    def verify(self, pn: PhoneNumber) -> str:
        code = str(request_id(pn) % 10000)
        logging.debug("Generated an audio captcha for %s: %s", pn, code)
        return code

    def generate(self, pn: PhoneNumber) -> Tuple[str, str, bytearray]:
        data = self._captcha.generate(self.verify(pn))
        return "Please enter the number shown in the image.", "audio/wav", data
