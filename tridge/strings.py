import yaml


class Strings:

    def __init__(self, filename: str):
        with open(filename, "r") as f:
            self._data = yaml.safe_load(f)

    def string_for_lang(self, key: str, lang: str):
        return self._data['strings'][key][lang]

    def string_for_code(self, key: str, code: str):
        strings = []
        for lang in self._data['languages'][code]:
            strings.append(self.string_for_lang(key, lang))
        return " ".join(strings)
