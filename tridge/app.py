import phonenumbers
import logging
from flask import Flask, Response

from tridge.challenges.audio import AudioChallenge
from tridge.challenges.image import ImageChallenge
from tridge.challenges.maths import MathsChallenge
from tridge.pool import BridgePool

demo = BridgePool(["Bridge 1.1.1.1"])

app = Flask(__name__)

challenge = {
    "audio": AudioChallenge(),
    "image": ImageChallenge(),
    "maths": MathsChallenge()
}

pn = phonenumbers.parse("+447875800800")


@app.route("/challenge")
@app.route("/challenge/<challenge_type>")
def route_challenge(challenge_type: str = None) -> Response:
    c = challenge[challenge_type or "maths"].generate(pn)
    resp = Response(c[2], mimetype=c[1])
    return resp


@app.route("/verify/<code>")
def route_verify(code: str) -> Response:
    if challenge["maths"].verify(pn) == code.strip():
        return Response(str(demo.request_bridge(pn)), status=200)
    return Response(challenge["maths"].verify(pn), status=403)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    app.run(debug=True)
