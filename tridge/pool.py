from typing import List
from typing import Optional

from phonenumbers import PhoneMetadata, PhoneNumber
from phonenumbers import is_valid_number
from phonenumbers.carrier import name_for_number
from phonenumbers.geocoder import country_name_for_number

from utils import e164, request_id

TRANSPORTS: List[str] = [
    "obfs4"
]

PhoneMetadata.load_all()


class BridgeLineSyntaxError(RuntimeError):
    pass


class InvalidPhoneNumberError(RuntimeError):
    pass


class BridgePool:
    bridges: List[str]
    forbidden_carriers: List[str]
    forbidden_prefixes: List[str]

    def __init__(self, bridges: List[str],
                 forbidden_carriers: Optional[List[str]] = None,
                 forbidden_prefixes: Optional[List[str]] = None):
        self.bridges = bridges
        self.forbidden_carriers = []
        if forbidden_carriers is not None:
            self.forbidden_carriers.extend(forbidden_carriers)
        self.forbidden_prefixes = []
        if forbidden_prefixes is not None:
            self.forbidden_prefixes.extend(forbidden_carriers)
        self._validate()

    def _validate(self):
        for bridge in self.bridges:
            parts = bridge.split(" ")
            if len(parts) < 2:
                raise BridgeLineSyntaxError(f"No spaces found: {bridge}")
            if parts[0] != "Bridge":
                raise BridgeLineSyntaxError(f"Does not begin with Bridge: '{bridge}'")
            if ":" in parts[1]:
                if len(parts) > 2 and len(parts[2]) != 40:
                    raise BridgeLineSyntaxError(f"Fingerprint error: '{bridge}'")
            else:
                if parts[1] not in TRANSPORTS:
                    raise BridgeLineSyntaxError(f"Unexpected transport: '{bridge}'")
                if len(parts) < 3 or ":" not in parts[2]:
                    raise BridgeLineSyntaxError(f"No port number: '{bridge}'")
                if len(parts) > 3 and len(parts[3]) != 40:
                    raise BridgeLineSyntaxError(f"Fingerprint error: '{bridge}'")

    def request_bridge(self, pn: PhoneNumber):
        if not is_valid_number(pn):
            raise InvalidPhoneNumberError("Not a valid number.")
        cid = e164(pn)
        for forbidden_prefix in self.forbidden_prefixes:
            if cid[1:].startswith(forbidden_prefix):
                raise InvalidPhoneNumberError("Number prefix forbidden.")
        if name_for_number(pn, "en") in self.forbidden_carriers:
            raise InvalidPhoneNumberError("Number carrier forbidden.")
        selection = request_id(pn) % len(self.bridges)
        print(name_for_number(pn, "en"))
        print(country_name_for_number(pn, "en"))
        print(pn.country_code)
        return self.bridges[selection]
